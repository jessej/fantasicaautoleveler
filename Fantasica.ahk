#Include Lib/Gdip_All.ahk
#Include Lib/Screenshot.ahk
#Include Lib/SetToolTipFont.ahk

; -----------------------------------------------------------------------------
;                            FANTASICA AUTO LEVELER
; -----------------------------------------------------------------------------
#SingleInstance Force
SetTitleMatchMode, 2
SendMode, Event

; -----------------------------------------------------------------------------
;                                  CONSTANTS
; -----------------------------------------------------------------------------

O_VERTICAL          := "V"
O_HORIZONTAL        := "H"

SELL_NORMAL         := "S"
SELL_EXCHANGE       := "E"

QUEST_MODE_LEVELING := "L"
QUEST_MODE_OTHER    := "O"

; -----------------------------------------------------------------------------
;                                   CONFIG
; -----------------------------------------------------------------------------

ScrollMax           := 12 	; Start over after this many scrolling attempts
Leniency 			:= 80 	; Amount of color variation allowed in images

LoopTime			:= 600 	; Amount of sleep during each loop
ClickLength			:= 1200 ; Amount of time to hold clicks
MouseSpeed 			:= 4 	; Speed to move mouse (0-100)
DragSpeed           := 70
PageLoadTime        := 7000 ; Use this to as a generic page load timer
UnitPages           := 4    ; How many pages to go through before choosing the leader

AutoStart 			:= 1 	; Whether to start automatically
TrainingWaitTime	:= 24 * 60 ; Seconds to wait before training again
ArenaWaitTime       := 4 * 60 * 60 ; Seconds to wait before doing arena stuff again
TrainsBeforeSelling := 5    ; Times to train before selling/exchanging
ExchangeTimes       := 2    ; Times to cycle through exchanging units (10 units per cycle)
EuthanizeTime       := 10 * 60 ; How long to wait before killing itself
EuthanizeTime       := 10 * 60 ; How long to wait before killing itself

EnableLogging       := 1 	; Whether to log in txt file or not
EnableScaling       := 0 	; Whether to scale images (Not working yet)
EnableArena         := 0    ; Whether to even check for arena stuff

LogPath             := "C:\Users\Jesse\Dropbox\Temp" ; Directory of log file
IniFile             := "Fantasica.ini"

SellType            := SELL_EXCHANGE

; -----------------------------------------------------------------------------
;                                   QUEST CONFIG
; -----------------------------------------------------------------------------

QuestImage    := "Images/quest-current.bmp"
UnitPos1      := "648|288"
UnitPos2      := "726|297"
UnitPos3      := "776|281"
UnitPos4      := "842|265"
UnitPos5      := "924|291"

AllyPos1      := "648|469"
AllyPos2      := "726|469"
AllyPos3      := "776|469"
AllyPos4      := "842|469"
AllyPos5      := "924|469"

; -----------------------------------------------------------------------------
;                                   INITIALIZE
; -----------------------------------------------------------------------------

MinX 				:= 550 	; Rectangle of screen to search
MinY 				:= 0
MaxX 				:= 1050 
MaxY 				:= 900

ToolTipX 			:= 1060	; Position of Tooltip
ToolTipY 			:= 444

WholeScreenMode 	:= 0
BaseScreenWidth     := 1606
BaseScreenHeight    := 976
ScaleX              := 1
ScaleY              := 1
CenterX				:= MinX + ((MaxX - MinX) / 2)
CenterY 			:= MinY + ((MaxY - MinY) / 2)
OldLeniency         := -1   ; Used for temporarily adjusting image search stuff

LastToolTip         := ""

; -- Timing Info
ProgramStartTime    := A_Now
LoopStartTime 		:= A_Now
LastTrainTime 		:= 20130515230647
TrainsSinceSell     := 4
TotalRunningTime    := 0
LastRunningTimeVal  := A_Now
LastArenaRunTime    := A_Now
LastArenaStartTime  := A_Now
ArenaLength         := 7 * 24 * 60 * 60 ; 1 Week
QuestWaitTime       := 5 * 60

; -- Totals
BattleCount 		:= 0
TrainCount          := 0

; Current Unit Info
CurrentUnit         := 0
CurrentUnitExp      := 0
CurrentUnitBattles  := 0
CurrentUnitStars    := 0
CurrentUnitStartTime:= 20130515230647

; -- Annoying Dialog
AnnoyingDialogCount := 0
AddAnnoyingDialog("Images/failed-connection.bmp", 250, 150)
AddAnnoyingDialog("Images/launch-app.bmp")
AddAnnoyingDialog("Images/login-bingo-header.bmp")
AddAnnoyingDialog("Images/big-close.bmp")
AddAnnoyingDialog("Images/start-game.bmp")
AddAnnoyingDialog("Images/my-apps.bmp")

InvertQuestSearch = 0

; -- Storage Stuff
PersistantVariableCount = 0

; -- Other stuff
CloseAfterBattle = 0
PauseMode := False

; -- Shadowflame stuff
ShadowflameMode := True
LastShadowflameCitadelTime := 20130515230647
LastBattleOfHeroesTime := 20130515230647

; -- Quest stuff
QuestMode := QUEST_MODE_OTHER
QuestWait := 5 * 60
LastQuestTime := A_Now
UnitCursor    := 1

Initialize()

; -----------------------------------------------------------------------------
;                                    HOTKEYS
; -----------------------------------------------------------------------------

#IfWinActive BlueStacks

; Train ASAP
^t::
    LastTrainTime := 20130515230647
    MyToolTip()
Return

; Sell ASAP
^s:: 
	TrainsSinceSell := TrainsBeforeSelling
    MyToolTip()
Return

; Toggle sell type
^!s::
    If (SellType = SELL_NORMAL)
        SellType := SELL_EXCHANGE
    Else
        SellType := SELL_NORMAL
        
    MyToolTip()
Return

; Toggle Quest Mode
^!q::
    If (QuestMode = QUEST_MODE_LEVELING)
        QuestMode := QUEST_MODE_OTHER
    Else
        QuestMode := QUEST_MODE_LEVELING
        
    MyToolTip()
Return

; Toggle Shadowflame mode
^!f::
    ShadowflameMode := !ShadowflameMode
Return

; Exit app
^x::
    SaveData()
	ExitApp
Return 

; Exit after battle
^+x::
    CloseAfterBattle = 1
    MyToolTip()
Return

#IfWinExist, BlueStacks

^p::
    PauseMode := !PauseMode
Return

; Test Function

^!t::
    FindBattleOfHerosOpponent()
Return

; -----------------------------------------------------------------------------
;                                   FUNCTIONS
; -----------------------------------------------------------------------------

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                   Main Loop
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

StartGame(LaunchX, LaunchY)
{
	MyToolTip("Waiting for game to start...")
	
	MouseClick, Left, LaunchX, LaunchY, 400, 5
	
	Loop
	{
		Sleep, %LoopTime%
		
		Found := CheckImage("Images/home-screen.bmp")
		
		If (Found = 0)
		{
			Break
		}
		
		ClickNo()
		ClickStartGame()
		
		CheckAlive()
	}
	
	Sleep, 3000
	
    SaveData()
	Reload
}

MainLoop()
{
	Global
	
	Log("MainLoop Started")
	
	Loop
	{
        MainLoopInit()
        
        If (PauseMode)
        {
            PauseLoop()
        }
        Else If (ShadowflameMode)
        {
            ShadowflameLoop()
        }
        Else If (TrainsSinceSell >= TrainsBeforeSelling)
        {
            DoSelling()
            InvertQuestSearch = 1
        }
		Else If (GetElapsedSeconds(LastTrainTime) >= TrainingWaitTime)
		{
            DoTraining()
        }
        Else If (EnableArena && CheckArenaTime())
        {
            DoArena()
        }
		Else
		{
            DoQuest()
		}
    }
}

PauseLoop()
{
    Global
    
    SaveData()
    WinMinimize, BlueStacks

    TrainingPoppedUp := False
    
    ToolTip
    TrayTip, Fantasica Auto Leveler, Leveling is paused! Hit Ctrl + P to unpause the script
    
    Loop
    {
        If (!TrainingPoppedUp && GetElapsedSeconds(LastTrainTime) >= TrainingWaitTime)
        {
            TrainingPoppedUp := True
            TrayTip, Fantasica Auto Leveler, It's time to train!  Hit Ctrl + P to unpause the script
        }
        
        If (!PauseMode)
        {
            Reload
            Return
        }
            
        Sleep %LoopTime%
    }
}

MainLoopInit()
{
    Global LoopStartTime
    
    LoopStartTime := A_Now
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                 Clicking Shortcuts
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

ClickNo()
{
	MyToolTip("Clicking No")
	LongClickScaled(909, 516)
}
ClickYes()
{
	MyToolTip("Clicking Yes")
	LongClickScaled(695, 504)
}

ClickAdvance()
{
	MyToolTip("Clicking Advance")
	LongClickScaled(802, 856)
}

ClickMyPage()
{
	MyToolTip("Clicking MyPage")
	LongClickScaled(960, 855)
}

ClickUnitPosition()
{
	MyToolTip("Clicking Unit Position")
	LongClickScaled(823, 360)
}

ClickStartGame()
{
	MyToolTip("Clicking Start Game")
	LongClickScaled(752, 708)
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                  Training
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

DoTraining()
{
    Global LoopStartTime, LastTrainTime, TrainCount, TrainsSinceSell
    
    MyToolTip("Start training.", 1)
    
    TrainLoop()
    LastTrainTime := A_Now
    TrainCount += 1
    
    TrainsSinceSell += 1
    
    MyToolTip("Finished training in " . GetElapsedSeconds(LoopStartTime) . " seconds", 1)
}

TrainLoop()
{
	Global
	
	WaitForImage("Images/home-screen.bmp")
	
    ScrollClickImage("Images/training.bmp", 25, 25, 900, 850, O_HORIZONTAL, -260)
    
    WaitForImage("Images/training-header.bmp")
    Sleep, 1500
    
    TrainTillPointsGone()
}

TrainTillPointsGone()
{
    Loop
	{
        MyToolTip("Expending all training points")
        
		If (CheckImage("Images/no.bmp") = 0)
		{
			ClickNo()
			ClickMyPage()
			Return
		}
		
        If (CheckImage("Images/training-header.bmp") = 0)
        {
            SelectLatestTraining()
        }
        Else
        {
            ; Do a short click (trust me)
            Click, 802, 856
            LongClickScaled(802, 856)
            CheckAlive()
        }
	}
}

SelectLatestTraining()
{
    MyToolTip("Selecting latest training exercise")
    X  := 930 
    Y1 := 733
    Y2 := 510
    Y3 := 246
    Y4 := 602
    Y5 := 346
    
    Sleep 1000
    
    If (CheckImage("Images/training-select.bmp") = 0)
    {
        CheckAlive()
        LongClickScaled(974, 860)
        SelectLatestTraining()
        Return
    }
    
    ;Drag(697, 757, 697, 272)
    ;Drag(697, 757, 697, 272)
    
    Loop 5
    {
        Y := Y%A_Index%
        If (CheckTrainingSpot(X, Y))
        {
            Return
        }
    }
}

; Click this spot, and return true if it made the training header go away
CheckTrainingSpot(X, Y)
{
    LongClickScaled(X, Y)
    
    If (CheckImage("Images/training-header.bmp") = 1)
    {
        Return True
    }
    
    Return False
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                   Arena Stuff
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

CheckArenaTime()
{
    Global LastArenaStartTime, ArenaLength, ArenaWaitTime
    
    WithinArenaTime  := (GetElapsedSeconds(LastArenaStartTime) < ArenaLength)
    WaitedLongEnough := (GetElapsedSeconds(LastArenaRunTime) >= ArenaWaitTime)
    WithinLastTenMin := (ArenaLength - GetElapsedSeconds(LastArenaStartTime) < 600)
    Return (WithinArenaTime && (WaitedLongEnough || WithinLastTenMin))
}

DoArena()
{
    Global LastArenaRunTime, LoopStartTime, O_HORIZONTAL
    
    MyToolTip("Start arena loop.", 1)
    
    ; Choose Arena
    ScrollClickImage("Images/arena.bmp", 25, 25, 800, 850, O_HORIZONTAL, 180)
    
    ArenaLoop()
    LastArenaRunTime := A_Now
    
    MyToolTip("Finished arena stuff in " . GetElapsedSeconds(LoopStartTime) . " seconds", 1)
}

ArenaLoop()
{    
    ; Loop until out of tickets
    Loop
    {   
        MainLoopInit()
        
        WaitForImage("Images/arena-header.bmp")
        
        ; Check for tickets
        If (CheckImage("Images/no-tickets.bmp") = 0)
        {
            Log("Ending arena loop - no more tickets!")
            
            ; Click Back
            LongClickScaled(981, 862)
            Return
        }
        
        ; Click Tournament mode
        LongClickScaled(693, 317)
        Sleep, 1200

        ; Click Yes
        LongClickScaled(695, 542)
        
        FightTourneyLoop()
    }
}

FightTourneyBattle()
{
    ; Go here after winning
    WaitForImage("Images/tourney-header.bmp")
    Sleep, 2000
    
    ; Click Advance
    LongClickScaled(801, 860)
    Sleep, 2000
    
    ; Click Fight
    LongClickScaled(815, 576)
    
    ; Click Skip until arena is over
    Loop
    {
        If (CheckImage("Images/fight-results.bmp") = 0)
        {
            Break
        }
        
        LongClickScaled(995, 96)
        CheckAlive()
    }
    
    ; Log outcome
    If (CheckImage("Images/arena-victory.bmp") = 0)
    {
        Log("You won the Arena battle.")
    }
    Else
    {
        Log("You've been defeated in the Arena.")
    }
}

FightTourneyLoop()
{
    Loop 3
    {
        Log("Starting a battle in Arena")
        FightTourneyBattle()
        
        ; Click Advance
        LongClickScaled(801, 860)
        
        ; Wait for header
        WaitForImage("Images/tourney-header.bmp")
        
        Sleep, 1000
        
        If (CheckImage("Images/tourney-back.bmp") = 0)
        {
            ; Click back
            LongClickScaled(812, 563)
            Return
        }
    }
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                            Quests (Unit leveling)
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

DoQuest(FromHomeScreen = 1)
{
    Global O_HORIZONTAL, InvertQuestSearch
    MyToolTip("Starting Quest", 1)
    WaitForImage("Images/home-screen.bmp")
    
    ; Makes fighting after selling quicker
    If (InvertQuestSearch = 1)
    {
        ScrollClickImage("Images/quests.bmp", 25, 25, 800, 850, O_HORIZONTAL, 180)
        InvertQuestSearch = 0
    }
    Else
    {
        ScrollClickImage("Images/quests.bmp", 25, 25, 900, 850, O_HORIZONTAL, -180)
    }
    
    
    StartBattle()
}

StartBattle()
{
	Global
	
	Loop
	{
        
		SetTemporaryLeniency(0)
		WaitForImage("Images/quest-header.bmp")
		RevertLeniency()
        
        ; Is it time to quest yet?
        If (QuestMode = QUEST_MODE_OTHER)
        {
            ; WaitToStartBattle()
            WaitForSomething("start battle", QuestWaitTime, LastQuestTime, True)
            ScrollClickImage(QuestImage, 375, 109, 798, 757, O_VERTICAL, -300)
        }
        Else
        {
            ScrollClickImage("Images/quest-two-seven.bmp", 25, 25, 798, 757, O_VERTICAL, -590)
        }
        
        LastQuestTime := A_Now
		
        If (QuestMode = QUEST_MODE_LEVELING)
        {
            SelectLevelingUnit()
            PositionUnit()
            UnitsDeployed := DeployAllies()
        }
        Else
        {
            PlaceSomeUnits(2)
            UnitsDeployed := DeployAllies()
            PlaceSomeUnits(3)
            UnitCursor := 1
        }
        
        
        
		WaitForBattle()	
		
        EndBattle(UnitsDeployed)
        
		If (PauseMode || GetElapsedSeconds(LastTrainTime) >= TrainingWaitTime)
		{	
			ClickMyPage()
			Return
		}
        
        ; Click Choose quest
        LongClickScaled(677, 853)
        
        ; Re-init loop since we're skipping ahead
        MainLoopInit()
	}

}

WaitForSomething(ActionName, WaitTime, LastOccurrence, DoTraining = False)
{
    Loop
    {
        ; If it's time to train, abort loop and do that
        If (DoTraining && GetElapsedSeconds(LastTrainTime) >= TrainingWaitTime)
		{
            ClickMyPage()
            MainLoop()
            Return
        }
        
        TimeToWait := WaitTime - GetElapsedSeconds(LastOccurrence)
        
        If (TimeToWait > 0)
        {
            MyToolTip("Waiting " FormatSeconds(TimeToWait) " seconds to " ActionName "...")
        }
        Else
        {
            Return
        }
        Sleep 1000
    }
}

PlaceSomeUnits(MaxUnits)
{
	Global

    WaitForImage("Images/deploy-unit.bmp")
    
    MyToolTip("Clicking the center, waiting to start battle.")
    ; Possibly do this twice?
    Loop 4
    {
        LongClickScaled(752, 331)
        Sleep 1000
    }
    
    Loop
    {
        ; Click deploy-unit if its there, otherwise return
        If (UnitCursor > MaxUnits)
            Return
        If (ClickImage("Images/deploy-unit.bmp") = 1)
            Return
        
        Sleep 1000
        
        ; Choose first unit
        MyToolTip("Choosing the first unit")
        LongClickScaled(941, 221)
        Sleep 1000
        
        ; Position unit
        MyToolTip("Positioning Unit " UnitCursor)
        X := GetXCoord(UnitPos%UnitCursor%)
        Y := GetYCoord(UnitPos%UnitCursor%)
        LongClickScaled(X, Y)
        Sleep 1000
        
        ; Click Confirm
        MyToolTip("Clicking Confirm")
        LongClickScaled(681, 859)
        Sleep 1000
        
        UnitCursor++
        
        CheckAlive()
    }
}

EndBattle(UnitsDeployed)
{
    Global
    
    BattleCount++
    
    CurrentUnitBattles++
    
    ExpGained := GetExperienceGained(UnitsDeployed)
    Efficiency := CalculateEfficiency(ExpGained, GetElapsedSeconds(LoopStartTime))
    
    CurrentUnitExp += ExpGained
    
    Log("Unit " . CurrentUnit . " gained " . ExpGained . " XP. (" . GetPercentLeveled() . "%) ~" . GetEstimatedBattlesRemaining() . " Battles Remain. Battle Efficiency: " . Efficiency . "%")
    
    If (CloseAfterBattle) {
        SaveData()
        WinClose, BlueStacks
        ExitApp
    }
}

WaitForBattle()
{
    ; Battle is raging onward!
    MyToolTip("Killing stuff!")
    Sleep, 9000
    MyToolTip("And more killing!")    
    Sleep, 9000
    MyTooltip("Delivering final blows.")
    Sleep, 4000

    SetTemporaryLeniency(0)
    
    Loop
    {
        ; Mash on center of screen
        Click, 781, 521
        If (CheckImage("Images/quest-results.bmp") = 0)
        {
            RevertLeniency()
            Break
        }
        
        Sleep, 25
        
        CheckAlive()
    }
}

; Put unit in the best spot
PositionUnit()
{
	Global
        
    ClickUnitPosition()
    
    Sleep, 1000
    
    ; Confirm
    LongClickScaled(681, 852)
    
    Sleep, %LoopTime%

}

; Waits for the BS in the beginning of battle, then clicks the deploy
; unit menu
SelectLevelingUnit()
{
	Global

	Loop
	{
		Sleep, %LoopTime%
		
		If (CheckImage("Images/deploy.bmp") = 0)
		{
			Sleep, 1000
			
			; This will be 1 if no unit was found
			UnitWasFound := GetFirstUnmaxedUnit()
			
			If (UnitWasFound = 0)
			{
				SelectLeader()
			}
			
			Return
		}
		Else
		{
			; Didn't see any deploy button, keep trying to open the menu
			LongClickScaled(673, 847)
		}
		
		CheckAlive()
	}
}

; Click previous button till you hit the first page, then 
; deploy that unit
SelectLeader()
{
    Global LoopTime, UnitPages
	Loop %UnitPages%
	{
		; Previous Page
		LongClickScaled(666, 786)
		Sleep, %LoopTime%
	}
	
	; Leader Unit position
	LongClickScaled(970, 215)
}

; Loop through all the pages of the deploy menu until
; you find a unit that is not maxed out
GetFirstUnmaxedUnit()
{
	Global CurrentUnit, UnitPages
    
	; Assumes there are 12 pages
	Loop %UnitPages%
	{
		Position := GetUnmaxedUnitPosition()
		
		; If all the units are maxed, go to the next page
		If (Position = 0)
		{
			LongClickScaled(933, 789)
			
			Sleep, 1000
			
			Continue
		}
		
		; Get coords of unmaxed unit
		UnitCoords := GetUnitCoords(Position)
		StringSplit, SearchRegion, UnitCoords,"|"
		X1 := SearchRegion1
		Y1 := SearchRegion2
		
		; Click on the button
		ClickX := X1 + 411
		ClickY := Y1 + 85
		
		UnitIndex := (A_Index - 1) * 4 + Position
        
        If (UnitIndex != CurrentUnit)
        {
            ResetCurrentUnitStats(UnitIndex)
        }
        
		LongClickScaled(ClickX, ClickY)
		
		Return 1
	}
}

; Loop through the four units on the screen, return the first
; one that is not maxed out
GetUnmaxedUnitPosition()
{
	ReturnVal = 0

	; Loop through the four units displayed
	Loop, 4
	{
		UnitCoords := GetUnitCoords(A_Index)
		StringSplit, SearchRegion, UnitCoords,"|"
		X1 := SearchRegion1
		Y1 := SearchRegion2
		X2 := SearchRegion3
		Y2 := SearchRegion4
		
		ImageSearch, FoundX, FoundY, X1, Y1, X2, Y2, *90 Images/exp-max.bmp
		
		; Unit was not maxed
		If (ErrorLevel = 1)
		{
			ReturnVal := A_Index
			Break
		}
	}
	
	; This will still be zero if all units were maxed
	Return ReturnVal
}

; Get the coords of a unit's box on the deploy menu
GetUnitCoords(Position)
{
	; Region Top Left =    562, 132
	; Region Lower Right = 1044, 689
	; Unit Box Height = 557 / 4 ~ 140
	
	X1 := 562
	Y1 := 132 + (Position - 1) * 140
	X2 := 1044
	Y2 := 132 + Position  * 140
	
	Return X1 . "|" . Y1 . "|" . X2 . "|" . Y2
}

DeployAllies()
{
	Global
	
    MyToolTip("Deploying Allies")
    
    UnitsDeployed := 0
    MaxAllies := 4
    
	Loop %MaxAllies%
	{
		Sleep, %LoopTime%
		
        ; Click call Ally
        MyToolTip("Click Call Ally")
		LongClickScaled(923, 850)
		Sleep, 2000
		
		; Try clicking deploy ally button
		if (ClickImage("Images/deploy-ally.bmp") != 0)
        {
            ; No Deployable allies, click back
            MyToolTip("No allies left to deploy")
            LongClickScaled(996, 861)
            Break
        }
        
        If (QuestMode = QUEST_MODE_LEVELING)
        {
            ; Position Unit
            X := 662 + (A_Index - 1) * 65
            Y := 547
        }
        Else
        {
            ; Use the configed allies position
            X := GetXCoord(AllyPos%A_Index%)
            Y := GetYCoord(AllyPos%A_Index%)
        }
		
        MyToolTip("Positioning Ally " A_Index)
		LongClickScaled(X, Y)
		
		; Click Confirm
        MyToolTip("Clicking Confirm")
		LongClickScaled(685, 854)
		
        ; Tally that shit
        UnitsDeployed++
	}
    
    Return %UnitsDeployed%
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                  Selling Units
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

DoSelling()
{
    Global TrainsSinceSell, SellType, SELL_NORMAL
    
    
    
    ; ReceiveAllGifts()
    
    If (SellType = SELL_NORMAL)
    {
        SellAll()
    }
    Else
    {
        ExchangeAll()
    }
    
    TrainsSinceSell = 0
}

ReceiveAllGifts()
{
	Global
	
	WaitForImage("Images/home-screen.bmp")
	ScrollClickImage("Images/sell.bmp", 25, 25, 930, 850, O_HORIZONTAL, -260)
	WaitForImage("Images/gifts.bmp")
	
	Loop
	{
		MyToolTip("Receiving gifts")
		LongCLickScaled(980, 200)
		
		If(CheckImage("Images/receive-disabled.bmp") = 0)
		{
			ClickMyPage()
			Break
		}
	}
}

SellAll()
{
	Global
	
    MyToolTip("Selling everything", 1)
    
	WaitForImage("Images/home-screen.bmp")
	ScrollClickImage("Images/inbox.bmp", 25, 25, 930, 850, O_HORIZONTAL, -260)
	Sleep, %PageLoadTime%
	
	MyToolTip("Selecting units to sell")
	
	Loop
	{
		LongClickScaled(974, 255)
		LongClickScaled(974, 393)
		LongClickScaled(974, 538)
		LongClickScaled(974, 684)
		Drag(779, 757, 779, 188)
	
		If (CheckImage("Images/unselected.bmp") = 1)
		{
			MyToolTip("Selling Units")
			LongClickScaled(870, 850)
			Sleep, %PageLoadTime%
			LongClickScaled(676, 863)
			Sleep, %PageLoadTime%
			LongClickScaled(988, 859)
			
			Break
		}
	}
    
    MyToolTip("Finished selling", 1)
}

ExchangeAll()
{
	Global
	
    MyToolTip("Exchanging everything", 1)
    
	WaitForImage("Images/home-screen.bmp")
	ScrollClickImage("Images/exchange.bmp", 25, 25, 860, 850, O_HORIZONTAL, -260)
	Sleep, %PageLoadTime%
	
    ; Exchange cards 3 times (May need tweaking)
    Loop %ExchangeTimes%
    {
        ; Click Cards
        LongClickScaled(659, 216)
        Sleep, %PageLoadTime%
        
        MyToolTip("Selecting units to exchange")
        
        ; Max 10 Units
        Loop 3
        {
            LongClickScaled(974, 255)
            LongClickScaled(974, 393)
            
            If (A_Index < 3)
            {
                LongClickScaled(974, 538)
                LongClickScaled(974, 684)
                Drag(779, 757, 779, 190)
            }
            
        }
        
        ; Don't Attempt to exchange units if the exchange button is disabled
        If (CheckImage("Images/exchange-disabled.bmp") = 0)
        {
            ; Back
            LongClickScaled(988, 859)
            Sleep, %PageLoadTime%
            Break
        }
        
        MyToolTip("Selling Units")
        LongClickScaled(870, 850)
        Sleep, %PageLoadTime%
        LongClickScaled(676, 863)
        Sleep, %PageLoadTime%
    }
    
    ; Back
    LongClickScaled(988, 859)
    
    MyToolTip("Finished exchanging", 1)    
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                  Shadowflame
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

ShadowflameLoop()
{
    MyToolTip("Entering Shadowflame mode!")
    
    Loop
    {
        If (ShadowflameMode = False)
        {
            Return
        }
        
        SelectShadowflameTab()
        
        DoShadowflameCitadelBattle(True)
        DoBattleOfHeroes()
        DoShadowflameCitadelBattle(False)
        DoBattleOfHeroes()
        
        MyToolTip("Going to do Training")
        WaitForImage("Images/shadowflame-header.bmp")
        LongClickScaled(968, 858)
        DoTraining()
    }
}

UseIceWater()
{

    MyToolTip("Attempting to use Ice Wate(L)")
    LongClickScaled(670, 644)
    Sleep 2000
    LongClickScaled(703, 532)
    Sleep 1000
    
    ; Use ice water
    MyToolTip("Attempting to use Ice Wate(S)")
    LongClickScaled(677, 565)
    Sleep 2000
    LongClickScaled(703, 532)
    Sleep 1000
}

SelectShadowflameTab()
{
	WaitForImage("Images/home-screen.bmp")
	
    ScrollClickImage("Images/shadowflame-icon.bmp", 25, 25, 900, 850, O_HORIZONTAL, -260)
    
    WaitForImage("Images/shadowflame-header.bmp")
}

DoShadowflameCitadelBattle(UseIceWater)
{
    Global LastShadowflameCitadelTime
    
    WaitForImage("Images/shadowflame-header.bmp")
    
    WaitTime = 615
    WaitForSomething("start Shadowflame Citadel", WaitTime, LastShadowflameCitadelTime, False)
    LastShadowflameCitadelTime := A_Now
    
    UseIceWater()
    
    MyToolTip("Clicking Shadowflame Citadel")
    LongClickScaled(695, 476)
    
    WaitForImage("Images/shadowflame-citadel-header.bmp")
    ScrollClickImage("Images/shadowflame-citadel-fight.bmp", 25, 25, 798, 757, O_VERTICAL, -300)
    
    ; Wait for battle to go on
    WaitForImage("Images/shadowflame-citadel-battle-results.bmp")
    LongClickScaled(968, 858)
    
    ; Wait for next screen
    WaitForImage("Images/shadowflame-citadel-header.bmp")
    LongClickScaled(968, 858)
    
    MainLoopInit()
}

DoBattleOfHeroes()
{
    Global LastBattleOfHeroesTime
    WaitForImage("Images/shadowflame-header.bmp")
    
    WaitTime = 615
    WaitForSomething("start Battle of Heroes", WaitTime, LastBattleOfHeroesTime, False)
    LastBattleOfHeroesTime := A_Now
    
    MyToolTip("Clicking Battle of Heroes")
    LongClickScaled(944, 478)
    
    WaitForImage("Images/shadowflame-battle-of-heroes-header.bmp")
    
    FindBattleOfHerosOpponent()
    
    WaitToClickImage("Images/shadowflame-battle-of-heroes-battle-button.bmp")
    
    ; Wait for battle to go on
    WaitForImage("Images/shadowflame-citadel-battle-results.bmp")
    LongClickScaled(968, 858)
    
    WaitForImage("Images/shadowflame-battle-of-heroes-header.bmp")
    LongClickScaled(968, 858)
    
    MainLoopInit()
}

FindBattleOfHerosOpponent()
{
    ; Loop through top three spots, select one that is 4
    ; stars or less.  Click search again if there isn't one
    ; Don't do any scrolling to keep it simple
    
    MyToolTip("Searching for a suitable opponent")
    Sleep, 500
    
    If (CheckHeroSpot(560, 255, 660, 280))
        LongClickScaled(971, 299)
    Else If (CheckHeroSpot(560, 480, 660, 490))
        LongClickScaled(971, 506)
    Else If (CheckHeroSpot(560, 670, 660, 705))
        LongClickScaled(971, 299)
    Else
    {
        ; Click Search
        MyToolTip("Clicking Search Again")
        LongClickScaled(804, 860)
        Sleep, 5000
        FindBattleOfHerosOpponent()
    }
}

; Return true if the area doesn't contain 5 or 6 star images
CheckHeroSpot(X1, Y1, X2, Y2)
{
    GoodSpot := True
    
    ImageSearch, FoundX, FoundY, X1, Y1, X2, Y2,*100 Images/shadowflame-five-star.bmp
    If ErrorLevel = 0
        GoodSpot := False
        
    ImageSearch, FoundX, FoundY, X1, Y1, X2, Y2,*100 Images/shadowflame-six-star.bmp
    If ErrorLevel = 0
        GoodSpot := False
        
    Return GoodSpot
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                 Time Stuff
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

GetElapsedSeconds(StartTime)
{
	ElapsedTime = 
	EnvSub, ElapsedTime, %StartTime%, S
	
	Return %ElapsedTime%
}

FormatSeconds(NumberOfSeconds)  ; Convert the specified number of seconds to hh:mm:ss format.
{
    time = 20010101 ;1/1/2001

    time += NumberOfSeconds, seconds

    FormatTime, d, %time%, d

    FormatTime, HHmmss, %time%, HH:mm:ss

    Return, (--d) " days " HHmmss
}

GetAsciiProgress(Part, Whole, TotalColumns = 20, FillChar = "|", EmptyChar = "-", Prefix = "[", Suffix = "]")
{
	Global
	
	Output := Prefix
	Percent := Part / Whole
	
	Filled := Floor(TotalColumns * Percent)
	
	If (Filled > TotalColumns)
	{
		Filled := TotalColumns
	}
    
	Empty := TotalColumns - Filled
	
	Loop, %Filled%
	{
		Output := Output . FillChar
	}
	
	Loop, %Empty%
	{
		Output := Output . EmptyChar
	}
	
	Output := Output . Suffix	
	
	Return Output
}

; -----------------------------------------------------------------------------
;                                    HOOKS
; -----------------------------------------------------------------------------

; Add this to the tooltip that always pops up
GetStats()
{
	Global
    
    Message := ""
    	
    If (CloseAfterBattle)
        Message := Message . " ------- Closing after next battle "
    Else If (PauseMode)
        Message := Message . " ------- Pausing after current main objective "
    Else If (ShadowflameMode)
        Message := Message . " ------- Shadowflame Mode "
    
    Message := Message . "`n*-*-*-*-*-*-*-     TOTALS       *-*-*-*-*-*-*"
    Message := Message . "`n"
	Message := Message . "`n        Total Battles: " . BattleCount
	Message := Message . "`n          Train Count: " . TrainCount
    Message := Message . "`n   Total Running Time: " . FormatSeconds(TotalRunningTime)
    Message := Message . "`n"
    
	Message := Message . "`n*-*-*-*-*-*-*-   CURRENT LOOP   *-*-*-*-*-*-*"
    Message := Message . "`n"
    Message := Message . "`n    Current Loop Time: " . FormatSeconds(GetElapsedSeconds(LoopStartTime))
	Message := Message . "`n     Current Run Time: " . FormatSeconds(GetElapsedSeconds(ProgramStartTime))
	Message := Message . "`n          Train Timer: " . GetAsciiProgress(GetElapsedSeconds(LastTrainTime), TrainingWaitTime)
    If (SellType = SELL_NORMAL)
        Message := Message . "`n            Sell Wait: " . GetAsciiProgress(TrainsSinceSell, TrainsBeforeSelling)
    Else
        Message := Message . "`n        Exchange Wait: " . GetAsciiProgress(TrainsSinceSell, TrainsBeforeSelling)
    If (EnableArena)
    {
        Message := Message . "`n           Arena Wait: " . GetAsciiProgress(GetElapsedSeconds(LastArenaRunTime), ArenaWaitTime)
        Message := Message . "`n"
    }
    
    If (QuestMode = QUEST_MODE_LEVELING)
        Message := Message . "`n           Quest Mode: Leveling"
    Else
        Message := Message . "`n           Quest Mode: Other"
    
    Message := Message . "`n"
	Message := Message . "`n*-*-*-*-*-*-*-   CURRENT UNIT   *-*-*-*-*-*-*"
    Message := Message . "`n"
    Message := Message . "`nCurrent Unit Progress: " . GetAsciiProgress(CurrentUnitExp, GetCurrentUnitMaxExp())
    Message := Message . "`n         Current Unit: " . CurrentUnit
    Message := Message . "`n   Current Unit Stars: " . CurrentUnitStars
    Message := Message . "`n     Current Unit Exp: " . CurrentUnitExp
    Message := Message . "`n    Current Unit Time: " . FormatSeconds(GetElapsedSeconds(CurrentUnitStartTime))
    Message := Message . "`n Current Unit Battles: " . CurrentUnitBattles
    Message := Message . "`n  Est. Battles Remain: " . GetEstimatedBattlesRemaining()
	
	Return Message
}

; Hook for when a dialog is found
HandleAnnoyingDialogFound(ImgSrc, FoundX, FoundY)
{
	Global MinX, MinY, MaxX, MaxY, LoopTime
	
	If (ImgSrc = "Images/failed-connection.bmp")
	{
		Log("Internet connection fucked up.")
	}
	Else If (ImgSrc = "Images/launch-app.bmp")
	{
		StartGame(FoundX, FoundY)
	}
    Else If (ImgSrc = "Images/start-game.bmp")
    {
        SaveData()
        Reload
    }
	Else If (ImgSrc = "Images/login-bingo-header.bmp")
	{		
		ImageSearch, FoundX, FoundY, MinX, MinY, MaxX, MaxY, *70 Images/game-tile.bmp
		If (ErrorLevel = 0)
		{
			LongClick(FoundX + 25, FoundY + 25)
			
			Sleep, %PageLoadTime%
			
			; Click Receive a few times
			Loop 4
			{
				LongClickScaled(805, 510)
				Sleep, %LoopTime%
			}
			
			ClickMyPage()
		}
		Else
		{
			Log("Couldn't find any tiles in Login Bingo!!!!")
		}
	}
}

; -----------------------------------------------------------------------------
;                       RANDOM ANNOYING DIALOG STUFF
; -----------------------------------------------------------------------------

; Add a dialog to the list of annoying dialogs to check for
AddAnnoyingDialog(ImgSrc, X = 25, Y = 25)
{
	Global
	
	; Add 1 first because ahk Arrays are 1 indexed...
	AnnoyingDialogCount += 1
	
	; Package up the the information
	Data := ImgSrc . "|" . X . "|" . Y
	
	; Add that to the list of stuff
	AnnoyingDialogs%AnnoyingDialogCount% := Data
}

; Try to click any annoying dialogs that pop up
ClickAnnoyingDialogs()
{
	Global
	
	WholeScreenMode := 1
    SetTemporaryLeniency(0)
	
	; Loop through the annoying dialogs
	Loop %AnnoyingDialogCount%
	{
		; Extract the data
		Data := AnnoyingDialogs%A_Index%
		StringSplit, DataSplit, Data,|
		ImgSrc := DataSplit1
		X := DataSplit2
		Y := DataSplit3
		
        FoundX := 0
        FoundY := 0
        MyImageSearch(FoundX, FoundY, ImgSrc)
        
		; Click it if its there
		If (ErrorLevel = 0)
		{
            ClickImage(ImgSrc, X, Y, 1)
			
			MyToolTip("Found annoying dialog: " . ImgSrc, 1)
			Sleep, %LoopTIme%
			
			; Comment this line if you don't use this function
			HandleAnnoyingDialogFound(ImgSrc, FoundX, FoundY)
			
			
		}
	}
	
	RevertLeniency()
    
	WholeScreenMode := 0
}

CheckAlive()
{
	Global EuthanizeTime, ProgramStartTime, LoopStartTime
    
    UpdateTotalRunningTime()
	
	WinActivate, BlueStacks
	
    ; Only check for dialogs after 90s
    If (GetElapsedSeconds(LoopStartTime) > 60)
    {
	    ClickAnnoyingDialogs()
    }
	
	MyToolTip()
	
    ; Maybe we're stuck on some page, try clicking back
	If (GetElapsedSeconds(LoopStartTime) > EuthanizeTime * 0.9)
	{
		ClickMyPage()
	}
	
	If (GetElapsedSeconds(LoopStartTime) > EuthanizeTime)
	{
        RunTime := FormatSeconds(GetElapsedSeconds(ProgramStartTime))
		Log("Program is not responsive after " . RunTime . ". Restarting...")
        SaveData()
		SaveScreenShot()
		RestartBlueStacks()
		Reload
	}
}

; -----------------------------------------------------------------------------
;                       LOGGING AND DISPLAYING INFORATION
; -----------------------------------------------------------------------------

UpdateTotalRunningTime()
{
    Global LastRunningTimeVal, TotalRunningTime
    
    TotalRunningTime += GetElapsedSeconds(LastRunningTimeVal)
    LastRunningTimeVal := A_Now
}

; Helper to show a message
MyToolTip(Message = "", LogMessage = 0)
{
	Global LastToolTip, ToolTipX, ToolTipY
	
	If (Message = "")
	{
		Message := LastToolTip
		LogMessage = 0
	}
	Else
	{
		LastToolTip := Message
	}
	
	If (LogMessage = 1)
	{
		Log(Message)
	}
	
	; Comment this line if you don't have a GetStats() Function
	Message .= "`n" . GetStats() 
	
	ToolTip, %Message%, %ToolTipX%, %ToolTipY%
}

Log(Message)
{
	Global EnableLogging, LogPath
	FormatTime,TimeString,,MM/dd/yyyy hh:mm:ss tt
	Message := TimeString . " - " Message . "`n"
	
	If (EnableLogging = 1)
	{
		FileAppend, %Message%, %LogPath%\log.txt
	}
}

SetPersistantVariables()
{
    MakePersistant("CurrentUnit")
    MakePersistant("CurrentUnitExp")
    MakePersistant("CurrentUnitBattles")
    MakePersistant("CurrentUnitStars")
    MakePersistant("CurrentUnitStartTime")
    
    MakePersistant("BattleCount")
    MakePersistant("TrainCount")
    MakePersistant("TotalRunningTime")
    
    MakePersistant("LastTrainTime")
    MakePersistant("TrainsSinceSell")
    MakePersistant("LastArenaRunTime")
    MakePersistant("LastArenaStartTime")
    
    MakePersistant("QuestMode")
    MakePersistant("LastQuestTime")
    
    MakePersistant("SellType")
    MakePersistant("ShadowflameMode")
    MakePersistant("LastShadowflameCitadelTime")
    MakePersistant("LastBattleOfHeroesTime")
}

MakePersistant(Variable)
{
    Global
    
    PersistantVariableCount++
    PersistantVariables%PersistantVariableCount% := Variable
}

SaveData()
{
    Global
    Loop %PersistantVariableCount%
    {
        MyIniWrite(PersistantVariables%A_Index%)
    }
}

LoadData()
{
    Global    
    Loop %PersistantVariableCount%
    {
        MyIniRead(PersistantVariables%A_Index%)
    }
}

MyIniWrite(Variable)
{
    Global IniFile
    
    ; Key is variable name
    Key := Variable
    
    ; Value is Value of variable
    Value := %Variable%
    
    IniWrite, %Value%, %IniFile%, Fantasica, %Key%
}

MyIniRead(Variable)
{
    Global
    
    ; Key is variable name
    Key := Variable
    
    ; Default Value is Value of variable as it is
    DefaultValue := %Variable%
    
    IniRead, OutputVar, %IniFile%, Fantasica, %Key%, %DefaultValue%
    
    %Variable% := OutputVar
}

SaveScreenShot()
{
	WinGetPos, X, Y, W, H, BlueStacks
	
	File := A_ScriptDir "\Screenshots\" A_Now ".png"
	Screen := X . "|" . Y . "|" . W . "|" . H
	Screenshot(File, Screen)
	IfExist, %File%
		Log("Screenshot file created:" . "\Screenshots\" A_Now ".png")
}

; -----------------------------------------------------------------------------
;                            BLUESTACKS MANAGEMENT
; -----------------------------------------------------------------------------

Initialize()
{
	Global AutoStart
	
    SetPersistantVariables()
    
	SetTooltipFont("normal s11", "Consolas")
	
	OpenBlueStacks()
	
	If (EnableScaling)
	{
		SetScale()
	}
    
    LoadData()
	
	If (AutoStart = 1)
	{
		MainLoop()
	}
}

OpenBlueStacks()
{
	IfWinNotExist, BlueStacks
	{
		Run, C:\Program Files (x86)\BlueStacks\HD-StartLauncher.exe
	}
	Else
	{
		WinActivate, BlueStacks
	}
	
	WinSet, Transparent, Off, BlueStacks
}

RestartBlueStacks()
{
	WinClose, BlueStacks
	Sleep, 5000
	OpenBlueStacks()
}

SetScale()
{
	Global
	
	WinGetPos, X, Y, Width, Height, BlueStacks
	ScaleX = Width / BaseScreenWidth
	ScaleY = Height / BaseScreenHeight
	
	MinX 		*= ScaleX
	MinY 		*= ScaleY
	
	MaxX 		*= ScaleX
	MaxY 		*= ScaleY
	
	ToolTipX 	*= ScaleX
	ToolTipY 	*= ScaleY
}

; -----------------------------------------------------------------------------
;                         GENERAL PURPOSE FUNCTIONS
; -----------------------------------------------------------------------------

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                           Clicking and Dragging
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  

; Move quickly to something, then do a nice long press
LongClick(X, Y)
{
	Global
	
	MouseMove, X, Y, MouseSpeed
	
	Click Down
	Sleep, %ClickLength%	
	Click Up
	Sleep, 75
}

; Long click somewhere, adjusted by the current scale
LongClickScaled(X, Y)
{
	Global EnableScaling
	
	If (EnableScaling)
	{
		X *= ScaleX
		Y *= ScaleY
	}
	
	LongClick(X, Y)
}

; Click somewhere and drag to another spot
Drag(StartX, StartY, EndX, EndY)
{
	Global
	
	If (EnableScaling)
	{
		StartX *= ScaleX
		StartY *= ScaleY
		EndX *= ScaleX
		EndY *= ScaleY
	}
	
	MouseMove, StartX, StartY, %MouseSpeed%
	Click Down
	
	Sleep, %ClickLength%
	
	MouseMove, EndX, EndY, %DragSpeed%
	
	Click Up
	
	Sleep, %LoopTime%
}

; Click an image.  This will always return the ErrorLevel, so that
; you can see if there was anything that got clicked
ClickImage(ImgSrc, SpaceX = 25, SpaceY = 25, SuppressMessage = 0)
{
	Global
	
	FoundX := 0
	FoundY := 0 
	
	MyImageSearch(FoundX, FoundY, ImgSrc)
	
	If (SuppressMessage = 0)
	{
		ShowImageSearchResult(ImgSrc, ErrorLevel) 
	}
	
	If (ErrorLevel = 0)
	{
		LongClick(FoundX + SpaceX, FoundY + SpaceY)
	}
	
	Return ErrorLevel
}

; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                              Image Searching
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  . 

; Search for an image, with scaling, and search rectangle defined
MyImageSearch(ByRef FoundX, ByRef FoundY, ImgSrc)
{
	Global
	
	If (EnableScaling)
	{
		GDIPToken := Gdip_Startup()                                     
		pBM := Gdip_CreateBitmapFromFile(imagefile)
		W := Gdip_GetImageWidth(pBM) * ScaleX
		H := Gdip_GetImageHeight(pBM) * ScaleY   
		Gdip_DisposeImage(pBM)                                          
		Gdip_Shutdown(GDIPToken)
		
		If (WholeScreenMode = 0)
		{
			ImageSearch, FoundX, FoundY, MinX, MinY, MaxX, MaxY, w%W% h%H% *%Leniency% %ImgSrc%
		}
		Else
		{
			WinGetPos, WinX, WinY, WinWidth, WinHeight, BlueStacks
			ImageSearch, FoundX, FoundY, 0, 0, WinWidth, WinHeight, w%W% h%H% *%Leniency% %ImgSrc%
		}
	}
	Else
	{
		If (WholeScreenMode = 0)
		{
			ImageSearch, FoundX, FoundY, MinX, MinY, MaxX, MaxY, *%Leniency% %ImgSrc%
		}
		Else
		{
			WinGetPos, WinX, WinY, WinWidth, WinHeight, BlueStacks
			ImageSearch, FoundX, FoundY, 0, 0, WinWidth, WinHeight, *%Leniency% %ImgSrc%
		}
	}
}

; Just return the ErrorLevel of an ImageSearch
CheckImage(ImgSrc)
{
	Global
	
	FoundX := 0
	FoundY := 0
	
	MyImageSearch(FoundX, FoundY, ImgSrc)
	
	Return ErrorLevel
}

; Keep on sleeping until you see the image you're looking for.
; Note that if any annoying dialogs you've set pop up at any time,
; this will click them
WaitForImage(ImgSrc, SuppressMessage = 0)
{
	Global
	
	Loop
	{
		CheckError := CheckImage(ImgSrc)

		If (SuppressMessage = 0)
		{
			ShowImageSearchResult(ImgSrc, ErrorLevel) 
		}
		
		If (CheckError = 0)
		{
			Break
		}
        Else
		{
			Sleep, %LoopTime%
		}
		
		CheckAlive()
	}
}

; Wait until an image appears, then click it
WaitToClickImage(ImgSrc, SpaceX = 25, SpaceY = 25)
{
	WaitForImage(ImgSrc)
	ClickImage(ImgSrc, SpaceX, SpaceY, 1)
}

; Scrollthrough some stuff till you find an image, then click it
ScrollClickImage(ImgSrc, SpaceX, SpaceY, StartX, StartY, Orientation, Distance)
{
	Global
    
	If (Orientation = O_HORIZONTAL)
	{
		EndX := StartX + Distance
		EndY := StartY
	}
	Else
	{
		EndX := StartX
		EndY := StartY + Distance
	}
	
	ScrollAttempts := 0
	
	Loop
	{
		MyToolTip("Searching for " . ImgSrc)
		
		If (ScrollAttempts >= ScrollMax)
		{
			Log("Couldn't find " . ImgSrc . ". Trying other direction.")
			ScrollClickImage(ImgSrc, SpaceX, SpaceY, EndX, EndY, Orientation, -Distance)
			Return
		}
		
		If (CheckImage(ImgSrc) = 0)
		{
			Break
		}
		
		; Drag and wait
		Drag(StartX, StartY, EndX, EndY)
		
		CheckAlive()
		
		ScrollAttempts += 1
	}
	
	ClickImage(ImgSrc, SpaceX, SpaceY) = 0
    Sleep, 500
}

; Show a tooltip for an ImageSearch
ShowImageSearchResult(ImgSrc, CheckError)
{
	If (CheckError = 0)
	{
		MyToolTip("Found " . ImgSrc)
	}
	Else If (CheckError = 1)
	{
		MyToolTip("Didn't find " . ImgSrc)
	}
	Else
	{
		MyToolTip("Image is broken: " . ImgSrc, 1)
	}
}

SetTemporaryLeniency(NewLeniency)
{
    Global OldLeniency, Leniency
    
    If (OldLeniency = -1)
    {
        OldLeniency := Leniency
    }
    
    Leniency := NewLeniency
}

RevertLeniency()
{
    Global OldLeniency, Leniency
    If (OldLeniency != -1)
    {
        Leniency := OldLeniency
    }
    
    OldLeniency := -1
}


; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                  Level Calculation
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  . 

GetExperienceGained(UnitsDeployed)
{
    Global CurrentUnitExp
    
    BaseExp        := 220
    Modifier       := 1 + (UnitsDeployed * 0.1)
    Return (Floor(BaseExp * Modifier))
}

ResetCurrentUnitStats(UnitIndex)
{
    Global
    
    Log(DumpUnitStats(CurrentUnit, UnitIndex))
    
    CurrentUnit          := UnitIndex
    CurrentUnitExp       := 0
    CurrentUnitBattles   := 0
    CurrentUnitStars     := 4
    CurrentUnitStartTime := A_Now
}

DumpUnitStats(OldUnit, NewUnit)
{
    Global
    
    Message := "Switched From Unit " . OldUnit . " to " . NewUnit . " at Stars: " . CurrentUnitStars . ", Exp: " . CurrentUnitExp . "/" . GetCurrentUnitMaxExp() . ", Battles: " . CurrentUnitBattles . ", Time: " . FormatSeconds(GetElapsedSeconds(CurrentUnitStartTime))
    
    Return Message
}

GetCurrentUnitMaxExp()
{
    Global CurrentUnitStars
    If (CurrentUnitStars = 1)
    {
        CurrentUnitMaxExp = 10450
    }
    Else If (CurrentUnitStars = 2)
    {
        CurrentUnitMaxExp = 42660
    }
    Else If (CurrentUnitStars = 3)
    {
        CurrentUnitMaxExp = 100570
    }
    Else If (CurrentUnitStars = 4)
    {
        CurrentUnitMaxExp = 191600
    }
    Else If (CurrentUnitStars = 5)
    {
        CurrentUnitMaxExp = 331150
    }
    Else If (CurrentUnitStars = 6)
    {
        CurrentUnitMaxExp = 563900
    }
    
    Return CurrentUnitMaxExp
}

GetEstimatedBattlesRemaining()
{
    Global CurrentUnitExp
    
    AvgExpPerBattle := 275
    Return Ceil((GetCurrentUnitMaxExp() - CurrentUnitExp) / AvgExpPerBattle)
}

GetPercentLeveled()
{
    Global CurrentUnitExp
    Return Round((CurrentUnitExp / GetCurrentUnitMaxExp()) * 100, 1)
}

CalculateEfficiency(ExpGained, LoopTime)
{
    OptimalSeconds := 60
    OptimalExp := 308
    
    ExpEfficiency := ExpGained / OptimalExp
    TimeEfficiency := OptimalSeconds / LoopTime
    
    Return Round(((ExpEfficiency + TimeEfficiency) / 2) * 100, 1)
}


; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  
;                                      Helpers
; .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  .  . 

GetXCoord(Coords)
{
    StringSplit, CoordsSplit, Coords,"|"
    Return CoordsSplit1
}

GetYCoord(Coords)
{
    StringSplit, CoordsSplit, Coords,"|"
    Return CoordsSplit2
}