; This file requires Gdip_All.ahk
; #Include Gdip_All.ahk

ScreenshotFromClipboard(outfile) ; Save screenshot from clipboard.
{
	pToken := Gdip_Startup()

	pBitmap := Gdip_CreateBitmapFromClipboard()

	Gdip_SaveBitmapToFile(pBitmap, outfile, 100)
	Gdip_DisposeImage(pBitmap)
	Gdip_Shutdown(pToken)
}

Screenshot(outfile, screen) ; Save screenshot from defined coordinates.
{
	pToken := Gdip_Startup()
	raster := 0x40000000 + 0x00CC0020

	pBitmap := Gdip_BitmapFromScreen(screen,raster)

	Gdip_SaveBitmapToFile(pBitmap, outfile, 100)
	Gdip_DisposeImage(pBitmap)
	Gdip_Shutdown(pToken)
}