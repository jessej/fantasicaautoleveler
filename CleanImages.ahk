; Get a list of images being used in the program
ImageCount = 1
Loop, read, Fantasica.ahk
{
    StringGetPos, Start, A_LoopReadLine, Images/
    If (ErrorLevel = 0)
    {
        
        RegExMatch(A_LoopReadLine, "Images/.*\.bmp", ImageSrc)
        If (FoundPos != -1)
        {
            Images%ImageCount% := ImageSrc
            ImageCount++
        }
    }
}

UnusedCount := 0
; Loop through Images, and move unnecessary ones to Unused
Loop, Images/*.bmp
{
    If (!IsBeingUsed(A_LoopFileName))
    {
        FileMove, %A_WorkingDir%\Images\%A_LoopFileName%, %A_WorkingDir%\Unused Images\
        UnusedCount++
    }
}

; Remove images in Screenshots dir
FileDelete, %A_WorkingDir%\Screenshots\*

MsgBox, % UnusedCount " images moved to the unused folder.`nScreenshots directory cleaned out."

; Returns true if an image is being used
IsBeingUsed(ImgSrc)
{
    Global
    
    Loop %ImageCount%
    {
        If ("Images/" ImgSrc = Images%A_Index%)
        {
            Return True
        }
    }
    
    Return False
}